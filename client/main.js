import angular from 'angular';

import { Meteor } from 'meteor/meteor';

import { name as App } from '../imports/home';

function onReady() {
  angular.bootstrap(document, [App]);
}

angular.element(document).ready(onReady);
