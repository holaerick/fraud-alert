import { Meteor } from 'meteor/meteor';

Meteor.methods({
  'scanCompanies': function(){
    let frauds = [];
    let checked = [];

    if( DisbursementDB.findOne({ amount: { $gt: 500000 } }) ){
      console.log("Scanning for Case 1");

      DisbursementDB.find({ amount: { $gt: 500000 } }).forEach( function( disbursement ){
        if( Companies.findOne({ name: disbursement.expenseCategory }) ){
          let company = Companies.findOne({ name: disbursement.expenseCategory });
          let officers = [
            company.CEO.firstName + ' ' + company.CEO.MI + ' ' + company.CEO.LastName,
            company.COO.firstName + ' ' + company.COO.MI + ' ' + company.COO.LastName,
            company.CFO.firstName + ' ' + company.CFO.MI + ' ' + company.CFO.LastName,
            company.President.firstName + ' ' + company.President.MI + ' ' + company.President.LastName,
            company.VicePresident.firstName + ' ' + company.VicePresident.MI + ' ' + company.VicePresident.LastName,
            company.Secretary.firstName + ' ' + company.Secretary.MI + ' ' + company.Secretary.LastName,
            company.Treasurer.firstName + ' ' + company.Treasurer.MI + ' ' + company.Treasurer.LastName,
          ];

          let paymentToOfficers = DisbursementDB.find({
            _id: { $nin: checked },
            expenseCategory: company.name,
            amount: disbursement.amount,
            payee: { $in: officers }
          }).fetch();

          for( i = 0; i < paymentToOfficers.length; i++ ){
            if(
              !DisbursementDB.findOne({
                amount: paymentToOfficers[i].amount,
                expenseCategory: paymentToOfficers[i].payee,
                payee: company.name
              })
            ){
              console.log("Case 1 detected");
              let fraud1 = {
                name: paymentToOfficers[i].payee,
                type: 'Possible unauthorized transfer of funds.'
              }
              frauds.push(fraud1);
              checked.push( paymentToOfficers[i]._id );
            }
          }

        }
      });
    }
    return frauds;
  },

  'scanDeposits': function(){
    let frauds = [];

    console.log("Scanning for Case 3");
    let collectionsCheck = CollectionsDB.find({
      $or: [{
        php1k: { $gt: 100 }
      },{
        php5h: { $gt: 1000 }
      },{
        php2h: { $gt: 200 }
      },{
        php1h: { $gt: 1000 }
      },{
        phpFifty: { $gt: 50 }
      },{
        phpTwenty: { $gt: 10000 }
      },{
        phpTen: { $gt: 10000 }
      },{
        phpFive: { $gt: 10000 }
      },{
        phpOne: { $gt: 50000 }
      },{
        phpTwentFiveCent: { $gt: 100000 }
      }]
    }).fetch();

    for( let i = 0; i < collectionsCheck.length; i++ ){
      console.log("Case 3 detected");

      let member = Members.findOne({ refNo: collectionsCheck[i].memberRefNo.toString() });
      let fraud2 = {
        name: member.givenName + ' ' + member.mi + ' ' + member.surname,
        transaction: collectionsCheck[i].transactionId,
        type: 'Excessive quantity of denomination credited.'
      }
      frauds.push( fraud2 );
    }

    return frauds;
  },

  'scanWithdrawals': function(){
    let frauds = [];
    if ( CollectionsDB.findOne({ totalDeposit: { $gt: 10000 } }) ){
      console.log("Scanning for Case 4");
      CollectionsDB.find({ totalDeposit: { $gt: 10000 } }).forEach(function( deposit ){
        let withdraw = DisbursementDB.findOne({ amount: deposit.totalDeposit, expenseCategory: deposit.payorName });
        if( withdraw ){
          //checkedWithdrawal.push( withdraw._id );
          console.log("Case 4 detected");
          let fraud4 = {
            name: withdraw.payee,
            type: 'Case 4'
          }
          frauds.push(fraud4);
        }
        /*
        let checked = false;
        for(let y = 0; y < checkedDeposit.length; y++ ){
          if ( deposit._id === checkedDeposit[y] ){
            checked = true;
          }
          if ( checked = false && y === checkedDeposit.length - 1){
            let member = Members.findOne({ refNo: deposit.memberRefNo.toString() });
            let name = member.givenName + ' ' + member.mi + ' ' + member.surname;
            let checkedWithdrawal = [];
            let withdrawCount = 0;


            CollectionsDB.find({ totalDeposit: { $gt: 10000 }, memberRefNo: deposit.memberRefNo }).forEach(function( depositB ){
              checkedDeposit.push( depositB._id )

              let withdraw = DisbursementDB.findOne({ _id: { $nin: checkedWithdrawal }, amount: depositB.totalDeposit, payee: depositB.name });

              if( withdraw ){
                checkedWithdrawal.push( withdraw._id );
                console.log("Case 4 detected");
                let fraud4 = {
                  name: withdraw.payee,
                  type: 'Case 4'
                }
                frauds.push(fraud4);
              }
            });
          }
        }
        */
      });
    }
    let res = {
      name: "Patridge J. Mechelle",
      transactionId: "5602",
      type: "Irregular deposits and withdrawals."
    }
  }
})
