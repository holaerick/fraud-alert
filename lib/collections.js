import { Mongo } from 'meteor/mongo';

BankHistory = new Mongo.Collection('bankhistory');
CollectionsDB = new Mongo.Collection('collectionsdb');
DisbursementDB = new Mongo.Collection('disbursementdb');
Members = new Mongo.Collection('members');
Companies = new Mongo.Collection('companies');
Frauds = new Mongo.Collection('frauds');
