import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngSanitize from 'angular-sanitize';

import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';

import template from './home.html';

class App {
  constructor($scope, $reactive) {
    'ngInject';
    $reactive(this).attach($scope);
    this.results = new ReactiveVar([]);

    this.helpers({
      logs: () => this.results.get()
    });

  }
  scan(){
    let self = this;
    Meteor.call('scanCompanies', function(err, result){
      if(!err){
        console.log(result);
        for(let i = 0; i < result.length; i++){
          self.results.get().push(result[i]);
        }
      }
    });

    Meteor.call('scanDeposits', function(err, result){
      if(!err){
        console.log(result);
        for(let x = 0; x < result.length; x++){
          self.results.get().push(result[x]);
        }
      }
    });

    Meteor.call('scanWithdrawals', function(err, result){
      if(!err){
        console.log(result);
        self.results.get().push(result);
      }
    });
  }

  clear(){
    this.results.set([]);
  }
}
const name = 'app';

export default angular.module(name, [
  angularMeteor,
  ngSanitize
]).component(name, {
  template: template,
  controller: App
})
